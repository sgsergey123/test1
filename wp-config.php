<?php

// Configuration common to all environments
include_once __DIR__ . '/wp-config.common.php';

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'is]8DpRygej@tZ]NirtX7B}LeAinG&Tlqa{rI$|A}ILPde-c/GT#F?5z*gBPXZ2Q');
define('SECURE_AUTH_KEY',  '=IN#L-LZcLN-y[bE.7UH;|hM_4T_Q1[+,jfLvC>J.>E4*|.xFc0{b&f=zk.p8?+t');
define('LOGGED_IN_KEY',    'BtaejEpYv+GlgjPYBQc.*SdM3S)`hLg2XhhG=69LKK=*8O*KE=g6W2gB*#77C4|C');
define('NONCE_KEY',        '9S7}*nb?9C}p.F7i!r,Ms!]{JZs|=|p:,jO>V+f(r{`}{USdj$>0[0hq6daruE{`');
define('AUTH_SALT',        'Ai.H73&6T*7y5AgwCFXuQ7tYrT0!Ud{4+Mx]7fp?_WRT~Na.Xv%JW8QOb!=DHnQ!');
define('SECURE_AUTH_SALT', 'Ul6#MUkEsMs$:#Of~qpOP_7CpNT9t;Z{~EfD74]f[?u0=4;D[E^%[ `IImJZ.7c:');
define('LOGGED_IN_SALT',   '#U.lTyW;@zdhr=Gxyql$I<TPw +w=uo!7Oq?,@rI4qm<Rth+_$eus}oU^4+wgayP');
define('NONCE_SALT',       'vItkeY|r)&EH3qtds[bwP`FS`Q81U;L02zup[`a}X/~Gig4,H-`b:RQ0,mJ=>/y;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('VP_ENVIRONMENT', 'default');
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
